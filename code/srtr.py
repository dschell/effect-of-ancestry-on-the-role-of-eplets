import pandas as pd
import numpy as np
from os.path import exists
import os
import sys
import math 
import time
import pickle
import warnings
from itertools import product, combinations

IU_path = '../../../immunogenicity-utilities'
sys.path.append(IU_path)
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import ImmunogenicityUtilities as IU

from joblib import Parallel, delayed, parallel_config
from multiprocessing import Queue

from sklearn.linear_model import LinearRegression
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.model_selection import train_test_split

from sklearn.cluster import AgglomerativeClustering
import string
strip_lower = str.maketrans('', '', string.ascii_lowercase)

from lifelines import CoxPHFitter
from lifelines.utils import concordance_index

import scipy.stats as stats
import scipy.linalg as linalg

DON_HLA = ['DON_A1', 'DON_A2', 'DON_B1', 'DON_B2', 'DON_C1', 'DON_C2', 'DON_DQ1', 'DON_DQ2', 'DON_DR1', 'DON_DR2']
REC_HLA = ['REC_A1', 'REC_A2', 'REC_B1', 'REC_B2', 'REC_C1', 'REC_C2', 'REC_DQ1', 'REC_DQ2', 'REC_DR1', 'REC_DR2']
race_cols = ['DON_RACE_CODE', 'CAN_RACE_CODE']

NO_LOWER = True
VIABLE_RACE_CODES = ["AFA", "CAU", "HIS"]
VAR_CUTOFF = 0.001
SP_EPSILON = 1e-9

# Low to High helpers
alleleTypeList_dict = ["A","B","C","DQB1","DRB1"]
alleleTypeList = ["A","B","Cw","DQ","DR"]
loci = ["A","B","C","DQ","DR"]
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    allele_conversion_dict = IU.make_allele_conversion_dict(IU_path+'/Dictionaries/HLAConversionDataDictionary.csv')
    allele_mapping_dict = IU.make_allele_mapping_dict(IU_path+'/Dictionaries/HLAConversionDataDictionary.csv')
    # master_dict = IU.make_haplo_frequency_dict(IU_path+"/Dictionaries/freq_dic/",alleleTypeList_dict)
    master_dict = IU.make_haplo_frequency_dict("data/HLA_freq/",alleleTypeList_dict)

# Eplet Helpers
# melt_Df = pd.read_csv(IU_path+"/Dictionaries/Eplet_Allele_ALL_Drop_NA_Melt.csv")
# eplet_dict, eplet_type_dict = EU.build_dictionaries(melt_Df)
with open("../../eplet_dict_epregistry.pkl", 'rb') as f:
        eplet_dict = pickle.load(f)

with open("../../abv_list_epregistry.pkl", 'rb') as f:
        abv_list = pickle.load(f)

race_codes = {  "Black or African American":                 "AFA", 
                "Arab or Middle Eastern":                    "CAU", 
                "Hispanic/Latino":                           "HIS",
                "American Indian or Alaska Native":          "NAM", 
                "Asian":                                     "API",
                "Native Hawaiian or Other Pacific Islander": "API", 
                "Indian Sub-continent":                      "API", 
                "White":                                     "CAU",
                "Generic":                                   "GEN",
                "":                                          "GEN"
        }

#
# Imputation
#

def get_high_hla_list(low_hla, mapping_dict, hla_dict):
    high_list = []
    if low_hla in mapping_dict:
        mapped_list = []
        for mapped_hla in list(mapping_dict[low_hla]) + [low_hla]:
            if mapped_hla in hla_dict:
                mapped_list.append(mapped_hla)
    else:
        mapped_list = [low_hla] if low_hla in hla_dict else []
    for low_hla in mapped_list:
        high_list.extend(hla_dict[low_hla])
    return list(set(high_list))

def unweighted_max_haplo_freq(one_haplo,current_haplo,freq_dic,mapping_dict, hla_dic):
    ret = []
    if type(freq_dic) == float:
        return(current_haplo,freq_dic,freq_dic)
    if one_haplo == []:
        return ret
    for a in get_high_hla_list(one_haplo[0],mapping_dict,hla_dic):
        if a in freq_dic:
            val = unweighted_max_haplo_freq(one_haplo[1:],current_haplo + [a],freq_dic[a],mapping_dict, hla_dic)
            if type(val) == tuple:
                ret.append(val)
            if type(val) == list and val != []:
                ret += val

    if ret:
        return max(ret, key=lambda arr: arr[1])
    else:
        return

def unweightedLowToHigh(race, alleles,alleleTypeList,allele_conversion_dict,allele_mapping_dict,freq_dict):
    if race is None:
        return [None]*len(alleles)
    
    patient_allele_dict = {}
    for i in range(len(alleleTypeList)):
        patient_allele_dict[alleleTypeList[i]] = alleles[2*i:2*i+2]
    
    for alleleType in patient_allele_dict.keys():
#         for allele in patient_allele_dict[alleleType]:
        for i in range(len(patient_allele_dict[alleleType])):
            allele = patient_allele_dict[alleleType][i]
            if allele == "":
                continue
            if allele not in allele_conversion_dict and allele not in allele_mapping_dict:
                patient_allele_dict[alleleType][i] = "blank" + alleleTypeList_dict[alleleTypeList.index(alleleType)]
#                 return [None]*len(alleles)
    
    combos = []
    masterlist = []
    for alleleType in patient_allele_dict.keys():
        masterlist.append(patient_allele_dict[alleleType])
        
    all_allele_combos = list(product(*masterlist))
    mid = (len(all_allele_combos) + 1) // 2   
    for firstHaplo, secondHaplo in zip(all_allele_combos[:mid], all_allele_combos[::-1]):
        c1 = unweighted_max_haplo_freq(firstHaplo,[],freq_dict[race], allele_mapping_dict, allele_conversion_dict)
        c2 = unweighted_max_haplo_freq(secondHaplo,[],freq_dict[race], allele_mapping_dict, allele_conversion_dict)
        
        # Why do we do this?
        if c1 and c2:
            if c1[0] == c2[0]:
                combos.append([c1[0]+c2[0], c1[1]*c2[1]])
            else:
                combos.append([c1[0]+c2[0], 2*c1[1]*c2[1]])
    if len(combos) == 0:
        if race == "GEN":
            return [None]*len(alleles)   
        else:
            return unweightedLowToHigh("GEN", alleles,alleleTypeList,allele_conversion_dict,allele_mapping_dict,freq_dict)
    

    return max(combos, key=lambda arr: arr[1])[0]

def unweighted_high_resolution(race,alleles,alleleTypeList, allele_mapping_dict, allele_conversion_dict, haplo_freq_dict):
    return_list = []

    # Making HLAs well-formed
    haplos = []
    for i in range(len(alleleTypeList)):
        for j in alleles[2*i:2*i+2]:
            if not math.isnan(j):
                haplos.append(str(alleleTypeList[i]) + str(int(j)))
            else:
                haplos.append("blank"+str(alleleTypeList[i]))   

    # Returning empty record, if no mapping found in dictionary for none of the HLAs in haplotype
    # If blank then hapl evals to false so this works but needs cleaned up later
#     if any(hapl and hapl not in allele_conversion_dict and hapl not in allele_mapping_dict for hapl in haplos):
#         return [None] * len(alleleTypeList)*2

    # Separating two haplotypes
    two_haplos = []
    two_haplos.append(IU.compare_haplo_blanks(haplos)[::2])
    two_haplos.append(IU.compare_haplo_blanks(haplos)[1::2])

#         Format two_haplos for new lowToHigh
    formated_haplos = [None]*(len(two_haplos[0])+len(two_haplos[1]))
    formated_haplos[::2] = two_haplos[0]
    formated_haplos[1::2] = two_haplos[1]

    race_convert = race_codes[race]
    best_candidate = unweightedLowToHigh(race_convert,formated_haplos,alleleTypeList,allele_conversion_dict,allele_mapping_dict,haplo_freq_dict)
    overall_list = []
    mid = (len(best_candidate) + 1) // 2   
    for x, y in zip(best_candidate[:mid], best_candidate[mid:]):
        overall_list.append(x)
        overall_list.append(y)
    
    return overall_list

def imputation_batch(sub_df, race_col, hla_cols, generic=False):
    if generic:
        return(sub_df.apply(lambda row: unweighted_high_resolution("Generic",row[hla_cols],alleleTypeList,allele_mapping_dict,allele_conversion_dict,master_dict),axis=1,result_type="expand"))       
    else:
        return(sub_df.apply(lambda row: unweighted_high_resolution(row[race_col],row[hla_cols],alleleTypeList,allele_mapping_dict,allele_conversion_dict,master_dict),axis=1,result_type="expand"))

def parallel_imputation(df, race_col, hla_cols, generic=False, n_jobs=10):
    sub_dfs = np.array_split(df, n_jobs)
    results = Parallel(n_jobs=n_jobs)(delayed(imputation_batch)(sub_df, race_col, hla_cols, generic) for sub_df in sub_dfs)
    return(pd.concat(results))

def get_eplets(row, loci, eplet_dict, hla_type = "I"):
    new_row = {}
    don = []
    rec = []

    prefix = ""
    for locus in loci:
        if (hla_type == "I" and "D" in locus) or (hla_type == "II" and "D" not in locus): continue
        if locus == "DR": prefix = "DR_"
        if locus == "DQ": prefix = "DQ_"
        if row['DON_'+locus+'1'] not in eplet_dict: print(row['DON_'+locus+'1'], "not found")
        if row['DON_'+locus+'2'] not in eplet_dict: print(row['DON_'+locus+'2'], "not found")
        if row['REC_'+locus+'1'] not in eplet_dict: print(row['REC_'+locus+'1'], "not found")
        if row['REC_'+locus+'2'] not in eplet_dict: print(row['REC_'+locus+'2'], "not found")
    
        don += [prefix + ep for ep in eplet_dict[row['DON_'+locus+'1']]] + [prefix + ep for ep in eplet_dict[row['DON_'+locus+'2']]]
        rec += [prefix + ep for ep in eplet_dict[row['REC_'+locus+'1']]] + [prefix + ep for ep in eplet_dict[row['REC_'+locus+'2']]]
        
    eplet_mismatches = set([ep for ep in don if ep not in rec])

    for ep in eplet_mismatches:
        new_row["E" + hla_type + "_" + str(ep)] = 1
    return new_row

#
# Weighted Correlation Network Analysis
#

def clusters_to_profiles(clustering, columns, no_lower = True):
    profiles = {}
    for c in range(len(clustering)):
        p = clustering[c] + 1
        if p in profiles:
            if no_lower:
                profiles[p].append(columns[c].translate(strip_lower))
            else:
                profiles[p].append(columns[c])
        else:
            if no_lower:
                profiles[p] = [columns[c].translate(strip_lower)]
            else:
                profiles[p] = [columns[c]]
    for p in [k for k in profiles.keys()]:
        if len(profiles[p]) <= 1: del profiles[p]
    return(dict(zip(range(1,len(profiles.keys())+1),profiles.values())))

def apply_profile(eplets_I, eplets_II, profiles_I, profiles_II):
    new_cols_I = {}
    drop_cols_I = []
    new_cols_II = {}
    drop_cols_II = []
    
    for profile in profiles_I:
        new_col = np.ones((eplets_I.shape[0]))
        eps = [e for e in profiles_I[profile] if e in eplets_I]
        for eplet in eps:
            new_col *= eplets_I[eplet]
        drop_cols_I += eps
        new_cols_I["Profile Type I " + str(profile)] = new_col
    
    for profile in profiles_II:
        new_col = np.ones((eplets_II.shape[0]))
        eps = [e for e in profiles_II[profile] if e in eplets_II]
        for eplet in eps:
            new_col *= eplets_II[eplet]
        drop_cols_II += eps
        new_cols_II["Profile Type II " + str(profile)] = new_col
    return (pd.concat((eplets_I.drop(drop_cols_I, axis=1), pd.DataFrame(new_cols_I)), axis=1), 
            pd.concat((eplets_II.drop(drop_cols_II, axis=1), pd.DataFrame(new_cols_II)), axis=1))

def get_profiles(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width):
    clustering_I  = AgglomerativeClustering(None, distance_threshold=cluster_width, 
                                            metric="precomputed", linkage="complete").fit(eplet_dist_I).labels_
    clustering_II = AgglomerativeClustering(None, distance_threshold=cluster_width, 
                                            metric="precomputed", linkage="complete").fit(eplet_dist_II).labels_
    
    profiles_I = clusters_to_profiles(clustering_I, eplets_I.columns)
    profiles_II = clusters_to_profiles(clustering_II, eplets_II.columns)
    return profiles_I, profiles_II

def get_profiled(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width = 0.01):
    profiles_I, profiles_II = get_profiles(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width)
    eplets_I_profiled, eplets_II_profiled = apply_profile(eplets_I, eplets_II, profiles_I, profiles_II)
    return(eplets_I_profiled.join(eplets_II_profiled))

def condition_number(df_cn):
    k_I = df_cn.corr()
    e_I, _ = np.linalg.eig(k_I)
    if np.min(e_I) > 0:
        return(np.max(e_I) / np.min(e_I))
    else:
        return(np.inf)

def correlation_rank(df_cr):
    k_I = df_cr.corr()
    e_I, _ = np.linalg.eig(k_I)
    return((e_I > 0).sum())

#
# Analysis
#

def parallel_cox(df_p, w_p, penalizer_p, df_p_test = None, fit_mode = "penalized", duration_col='surv', event_col='event', warm_start=None):
    # Required ridge for reliable convergence: 1/df_p.shape[0]
    cph_p = CoxPHFitter(penalizer=penalizer_p * w_p + 1/df_p.shape[0], l1_ratio=1-(1/df_p.shape[0]))
    cph_p.fit(df_p, duration_col=duration_col, event_col=event_col, initial_point=warm_start)
    if df_p_test is None:
        return (cph_p, penalizer_p)
    else:
        if fit_mode == "unpenalized":
            # Test against selected model without penalty
            dropped = cph_p.params_.index[cph_p.params_ < SP_EPSILON]
            df_post_train = recondition(df_p.drop(dropped, axis=1))
            cph_s = CoxPHFitter(penalizer=0)
            cph_s.fit(df_post_train, duration_col=duration_col, event_col=event_col, initial_point=warm_start)
            return cph_s.score(df_p_test[df_post_train.columns], scoring_method='concordance_index')
        else: # default to penalized
            return cph_p.score(df_p_test, scoring_method='concordance_index')

def cv_ridge_baseline(df_cv, fold):
    df_cv_train = df_cv[df_cv['fold'] != fold].drop(['fold'], axis=1)
    cv_std = df_cv_train.drop(['surv', 'event'], axis=1).std()
    cph_cv = CoxPHFitter(penalizer=1/df_cv_train.shape[0], l1_ratio=0)
    cph_cv.fit(df_cv_train, duration_col='surv', event_col='event')
    return(np.array(1/np.abs(cph_cv.params_*cv_std))) # Betas are params scaled by std

def recondition(df_train_c, tol=1e-9):
    immune = ['surv', 'event', 'likelihood']
    exclude = [c for c in immune if c in df_train_c.columns]

    t_corr = df_train_c.drop(exclude, axis=1).corr()
    cols = [i for i in t_corr.index]
    q, r, p = linalg.qr(t_corr, pivoting=True)
    keepers = [cols[i] for i in p[np.where(np.diag(r) > tol)[0]]]

    return(df_train_c[exclude + keepers])

def bootstrap_c_index(df_ci, cph_ci):
    boot_idx = np.random.choice([i for i in df_ci.index], size=df_ci.shape[0], replace=True)
    return(cph_ci.score(df_ci.loc[boot_idx].reset_index(drop=True), scoring_method='concordance_index'))

def fit_interaction(df_train_ir):
    np.random.seed(62675)

    # Remove low variance columns
    low_var = list(df_train_ir.columns[df_train_ir[df_train_ir['event']].var() < .015])
    low_var.remove('event')
    
    # Improve condition of matrix
    df_train_i = recondition(df_train_ir.drop(low_var, axis=1))
    train_std = df_train_i.drop(['surv', 'event'], axis=1).std()
    
    # Fit baseline ridge
    cph_w_i = CoxPHFitter(penalizer=1/df_train_i.shape[0], l1_ratio=0)
    cph_w_i.fit(df_train_i, duration_col='surv', event_col='event')
    w_i = np.array(1/np.abs(cph_w_i.params_*train_std))
    w_i[-1] = 0

    solution_path_i = [2 ** p for p in range(-19,1)]

    # Select penalizer via cross-validation
    FOLDS = 10
    fold = pd.Series(np.random.randint(FOLDS, size = df_train_i.shape[0]), index=df_train_i.index, name="fold")
    df_train_i = df_train_i.assign(fold = fold)
    
    w_cv = Parallel(n_jobs=10)(delayed(cv_ridge_baseline)(df_train_i, f) for f in range(FOLDS))
    
    penalty_quality_i = np.zeros((FOLDS, len(solution_path_i)))
    for f in range(FOLDS):
        print("Cross-validation fold", f)
        df_cv_train = df_train_i[df_train_i['fold'] != f].drop(['fold'], axis=1)
        df_cv_test = df_train_i[df_train_i['fold'] == f].drop(['fold'], axis=1)
        # df_cv_train = recondition(df_cv_train, tol=1e-9)
        col_idx = [df_train_i.drop(['surv','event'], axis=1).columns.get_loc(c) for c in df_cv_train.drop(['surv','event'], axis=1).columns]
        penalty_quality_i[f] = np.array(Parallel(n_jobs=10)(delayed(parallel_cox)(df_cv_train, w_cv[f][col_idx], s, df_cv_test[df_cv_train.columns]) for s in solution_path_i))

    df_train_i.drop(['fold'], axis=1, inplace=True)
    best_penalizer_i = solution_path_i[np.argmax(penalty_quality_i.mean(axis=0))]

    # Fit final model
    cph_best_i, _ = parallel_cox(df_train_i, w_i, best_penalizer_i)

    # Use bootstrap estimate of fit quality
    # B = 1000
    # bci = Parallel(n_jobs=10)(delayed(bootstrap_c_index)(df_test_i, cph_best_i) for b in range(B))
    # bci_interval_i = np.quantile(bci, (0.05, 0.5, 0.95))

    best_model_i = cph_best_i.params_[(np.abs(cph_best_i.params_) > SP_EPSILON)]

    return({
        'train_size':df_train_i.shape,
        # 'test_size':df_test_i.shape,
        'penalizer':best_penalizer_i,
        # 'bci': bci_interval_i,
        'model': best_model_i
    })

#
# Load and store datasets
#

def get_dataset(dataset_name = "gctc"):
    train_file = "data/" + dataset_name +"_train.parquet"
    test_file = "data/" + dataset_name +"_test.parquet"
    if exists(train_file) and exists(test_file):
        df_train = pd.read_parquet(train_file)
        df_test = pd.read_parquet(test_file)
    else:
        tx_ki = pd.read_sas("/home/kidneyData/DATA_SP_2020/tx_ki.sas7bdat", encoding='latin1')
        immuno = pd.read_sas("/home/kidneyData/DATA_SP_2020/immuno.sas7bdat", encoding='latin1')
        rec_histo = pd.read_sas("/home/kidneyData/DATA_SP_2020/rec_histo.sas7bdat", encoding='latin1')
        
        stats = {}
        ### Data filters
    
        if dataset_name == "contemporary":
            # From 1/1/2000 to 1/1/2021
            df = tx_ki.loc[(tx_ki['REC_TX_DT'] <= pd.Timestamp(2021,1,1)) &
                       (tx_ki['REC_TX_DT'] >= pd.Timestamp(2000,1,1)),].copy()
        else: # default to gctc
            # From 1/1/2000 to 1/1/2015
            df = tx_ki.loc[(tx_ki['REC_TX_DT'] <= pd.Timestamp(2015,1,1)) &
                       (tx_ki['REC_TX_DT'] >= pd.Timestamp(2000,1,1)),].copy()
            
        print("Initial: ", df.shape[0])
        stats["Initial"] = df.shape[0]
        cur_rows = df.shape[0]
        
        rec_histo_small = rec_histo[['REC_HISTO_TX_ID','DON_CW1','DON_CW2','DON_DQW1','DON_DQW2','REC_CW1','REC_CW2','REC_DQW1','REC_DQW2']]
        df = pd.merge(df, rec_histo_small, how="left", on="REC_HISTO_TX_ID")
        df = df.rename(columns={'DON_CW1': 'DON_C1', 'DON_CW2': 'DON_C2', 'DON_DQW1': 'DON_DQ1', 'DON_DQW2': 'DON_DQ2',
                                'REC_CW1': 'REC_C1', 'REC_CW2': 'REC_C2', 'REC_DQW1': 'REC_DQ1', 'REC_DQW2': 'REC_DQ2'
                               })
        print("With expanded HLA: ", df.shape[0])
        stats["Expanded HLA"] = df.shape[0]
        cur_rows = df.shape[0]
                 
        # Not primary failure
        df = df[df['REC_FAIL_CAUSE_TY'] != 103]
        print("Primary non-function: ", df.shape[0], cur_rows - df.shape[0])
        stats["Non-function"] = df.shape[0]
        cur_rows = df.shape[0]
        
        ### Clean race categories
        
        race_dict = {   8:    "White",
                        16:   "Black or African American",
                        32:   "American Indian or Alaska Native",
                        64:   "Asian",
                        128:  "Native Hawaiian or Other Pacific Islander",
                        256:  "Arab or Middle Eastern",
                        512:  "Indian Sub-continent",
                        2000: "Hispanic/Latino"
        }
    
        def updateRace(x):
            if x in race_dict.values():
                return x
            elif x in race_dict:
                return race_dict[x]
            else:
                return "Generic"
        # df.loc[:, ("DON_RACE")] = df["DON_RACE"].apply(lambda x: updateRace(x))
    
        df.loc[:,('DON_RACE')] = [updateRace(a) for a  in df["DON_RACE"]]
        df.loc[:,('CAN_RACE')] = [updateRace(a) for a  in df["CAN_RACE"]]
        
        don_race_idx = df.columns.get_loc('DON_RACE')
        don_hla_idx  = df.columns.get_loc('DON_A1')
        rec_race_idx = df.columns.get_loc('CAN_RACE')
        rec_hla_idx  = df.columns.get_loc('REC_A1')
        
        # Donor and recipient have some HLA measured default missing to White because imputation isn't better
        df = df[df[DON_HLA].notnull().any(axis=1) & df[REC_HLA].notnull().any(axis=1)]
        print("Valid HLA: ",df.shape[0], cur_rows - df.shape[0])
        stats["Valid HLA"] = df.shape[0]
        cur_rows = df.shape[0]
    
        if dataset_name == "contemporary":
            pass
        else: # default to gctc
            # No PRA allowed
            df = df[df['CAN_LAST_SRTR_PEAK_PRA'].notnull()]
            print("PRA present", df.shape[0], cur_rows - df.shape[0])
            stats["PRA"] = df.shape[0]
            cur_rows = df.shape[0]
            
            # No PRA allowed
            df = df[~(df['CAN_LAST_SRTR_PEAK_PRA'] > 0.0)]
            print("With PRA = 0.0", df.shape[0], cur_rows - df.shape[0])
            stats["PRA > 0"] = df.shape[0]
            cur_rows = df.shape[0]
        
    
        # Survived day of transplant
        df = df[(df['REC_TX_DT'] != df['TFL_GRAFT_DT'])]
        print("Survived day of transplant", df.shape[0], cur_rows - df.shape[0])
        stats["First Day"] = df.shape[0]
        cur_rows = df.shape[0]
    
        # Remove duplicates
        df.drop_duplicates(inplace=True)
        print("Deduplicated: ", df.shape[0], cur_rows - df.shape[0])
        stats["Deduplicated"] = df.shape[0]
        cur_rows = df.shape[0]
        
        with open('data/' + dataset_name + '_data_filters.pkl', 'wb') as fp:
            pickle.dump(stats, fp)
    
        # Time on dialysis (Highly suspect. Use with caution.)
        df['rec_dialysis_months'] = (df['REC_TX_DT'] - df['REC_DIAL_DT'])/np.timedelta64(1, 'W')/4 # Cannot measure ambiguous times like months
        
        ### Survival time and event determination
        
        # The event is return to dialysis or retransplantation (many alive with failed graft but not in this list)
        surv_cols = ['TFL_LAFUDATE', 'TFL_GRAFT_DT', 'REC_FAIL_DT', 'PERS_RELIST', 'PERS_RETX', 'REC_RESUM_MAINT_DIAL_DT']
        df['surv'] = (pd.to_datetime(df[surv_cols].min(axis=1)) - pd.to_datetime(df['REC_TX_DT'])) / np.timedelta64(1, 'D')
        df['event'] = (df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") | (df['TFL_GRAFT_DT'].notnull())
        print("Pure DCGF:", ((df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R")).sum())
        print("Total Events:", ((df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") | (df['TFL_GRAFT_DT'].notnull())).sum())
        # df['event'] = (df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") # DCGF
        # print("Mysterious graft failures:", (df['TFL_GRAFT_DT'].notnull() & ~df['TFL_DEATH_DT'].notnull() & ~df['event']).sum())
    
        df = df[['surv', 'event',
                 'DON_RACE', 'DON_A1', 'DON_A2', 'DON_B1', 'DON_B2', "DON_C1", "DON_C2", "DON_DQ1", "DON_DQ2", 'DON_DR1', 'DON_DR2', 
                 'CAN_RACE', 'REC_A1', 'REC_A2', 'REC_B1', 'REC_B2', "REC_C1", "REC_C2", "REC_DQ1", "REC_DQ2", 'REC_DR1', 'REC_DR2',
                 'REC_AGE_AT_TX', 'CAN_GENDER', 'REC_TX_DT', 'REC_DIAL_DT',
                 'REC_PRIMARY_PAY', 'DON_AGE', 'DON_GENDER', 'DON_TY', 'DON_EXPAND_DON_KI',
                 'DON_WGT_KG', 'CAN_WGT_KG', 'REC_COLD_ISCH_TM', 'rec_dialysis_months', 'TRR_ID']]
        df = df.loc[:,~df.columns.duplicated()].copy()
    
        imputable = ['DON_RACE', 'CAN_RACE', 'REC_AGE_AT_TX', 'CAN_GENDER', 'REC_PRIMARY_PAY', 'REC_AGE_AT_TX', 'DON_GENDER', 
                     'DON_TY', 'DON_EXPAND_DON_KI', 'DON_WGT_KG', 'CAN_WGT_KG', 'REC_COLD_ISCH_TM', 'rec_dialysis_months']
    
        print(df[imputable].isnull().sum())
        df.reset_index(inplace=True, drop=True)
    
        # Formerly the break between unquantized and quantized datasets
    
        # Identify categorical columns and make them numeric for the imputation
        is_object = df[imputable].dtypes == "object"
        need_dummies = is_object[is_object].index
        df_dummies = pd.get_dummies(df[need_dummies], drop_first = True)*1
        impute_raw = [c for c in imputable if c not in need_dummies]
        df_impute = df[impute_raw].join(df_dummies)
    
        # Multiple imputation via connected equations
        mice = IterativeImputer(estimator=LinearRegression(), imputation_order="roman")
        np_imputed = mice.fit_transform(df_impute)
        df_imputed = pd.DataFrame(np_imputed, columns = df_impute.columns)
        df_imputed = df_imputed.loc[:,~df_imputed.columns.duplicated()].copy()
    
        # Nothing categorical needed to be imputed
        for c in impute_raw:
            df[c] = df_imputed[c]
    
        df['REC_TX_DT'] = pd.to_datetime(df['REC_TX_DT'])
        
        ### Map recipient characteristics
        # Ages:
        df['rec_age_young']  = df['REC_AGE_AT_TX'] <= 14
        df['rec_age_mid']    = (df['REC_AGE_AT_TX'] > 14) & (df['REC_AGE_AT_TX'] <= 24)
        # rec_age_adult  = (df['REC_AGE_AT_TX'] > 24) & (df['REC_AGE_AT_TX'] <= 44) REF
        df['rec_age_golden'] = (df['REC_AGE_AT_TX'] > 44) & (df['REC_AGE_AT_TX'] <= 64)
        df['rec_age_senior'] =  df['REC_AGE_AT_TX'] > 64
    
        # Sex
        df['rec_sex'] = df['CAN_GENDER'] == 'F'
    
        # Race
        #rec_race_White = df['CAN_RACE'] == "White" REF
        df['rec_race_black'] = df['CAN_RACE'] == "Black or African American"
        df['rec_race_other'] = ~(df['CAN_RACE'] == "Black or African American") & ~(df['CAN_RACE'] == "White")
    
        # Insurance type
        df['rec_ins_private'] = df['REC_PRIMARY_PAY'] == 1 
        #rec_ins_public = df['REC_PRIMARY_PAY'].isin([2,3,4,5,6,7,12,13,14]) REF
        df['rec_ins_none'] = df['REC_PRIMARY_PAY'].isin([8,9,10,11,15])
    
    
        ### Map donor characteristics
    
        # Ages:
        # don_age_young  = df['DON_AGE'] <= 35 REF
        df['don_age_adult']  = (df['DON_AGE'] > 35) & (df['DON_AGE'] <= 45)
        df['don_age_golden'] = (df['DON_AGE'] > 45) & (df['DON_AGE'] <= 55)
        df['don_age_senior'] =  df['DON_AGE'] > 55
    
        # Sex
        df['don_sex'] = df['DON_GENDER'] == 'F'
    
        # Race
        #don_race_White = df['DON_RACE'] == "White" REF
        df['don_race_black'] = df['DON_RACE'] == "Black or African American"
        df['don_race_other'] = ~(df['DON_RACE'] == "Black or African American") & ~(df['DON_RACE'] == "White")
    
        # Donor type
        #don_type_living = df['DON_TY'] == "L" REF
        df['don_type_standard'] = (df['DON_TY'] == "C") & (df['DON_EXPAND_DON_KI'] == 0.0)
        df['don_type_expanded'] = (df['DON_TY'] == "C") & (df['DON_EXPAND_DON_KI'] == 1.0)
    
        ### Map Transplant characteristics 
        # Donor recipient weight ratio
        wr = df['DON_WGT_KG'].astype(float) / df['CAN_WGT_KG'].astype(float)
        # df['trans_wr_small'] = wr < .9
        df['trans_wr_large'] = wr > 1
        df['trans_wr_same'] = (wr >= .9) & (wr <= 1)
    
        # Transplant era
        # df['trans_era_early'] = df['REC_TX_DT'] < pd.Timestamp(2005,1,1) REF
        df['trans_era_mid']   = (df['REC_TX_DT'] >= pd.Timestamp(2005,1,1)) & (df['REC_TX_DT'] < pd.Timestamp(2010,1,1))
        df['trans_era_late']  = df['REC_TX_DT'] >= pd.Timestamp(2010,1,1)
    
        # Cold ischemic time
        # REC_COLD_ISCH_TM
    
        # Map drug types
        thymoglobulin = immuno[immuno['REC_DRUG_CD'] == 41][["TRR_ID"]]
        thymoglobulin["trans_induction_tmg"] = 1
    
        campath = immuno[immuno['REC_DRUG_CD'] == 50][["TRR_ID"]]
        campath["trans_induction_campath"] = 1
    
        IL2 = immuno[immuno['REC_DRUG_CD'].isin([20, 27])][["TRR_ID"]]
        IL2["trans_induction_IL2"] = 1
    
        cyclosporine = immuno[immuno['REC_DRUG_CD'].isin([-2, 46, 48])][["TRR_ID"]]
        cyclosporine["trans_calcineurin_cyclo"] = 1
    
        tacrolimus = immuno[immuno['REC_DRUG_CD'].isin([5, 53, 54, 59])][["TRR_ID"]]
        tacrolimus["trans_calcineurin_tacro"] = 1
    
        steroids = immuno[immuno['REC_DRUG_CD'] == 49][["TRR_ID"]]
        steroids["trans_steroids"] = 1
    
        df = pd.merge(df, thymoglobulin, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, campath, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, IL2, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, cyclosporine, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, tacrolimus, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, steroids, how='left', on='TRR_ID').drop_duplicates()
        df['trans_induction_tmg'].fillna(0, inplace=True)
        df['trans_induction_campath'].fillna(0, inplace=True)
        df['trans_induction_IL2'].fillna(0, inplace=True)
        df['trans_calcineurin_cyclo'].fillna(0, inplace=True)
        df['trans_calcineurin_tacro'].fillna(0, inplace=True)
        df['trans_steroids'].fillna(0, inplace=True)
    
        df['DON_RACE_CODE'] = df.apply(lambda row: race_codes[row['DON_RACE']], axis=1)
        df['CAN_RACE_CODE'] = df.apply(lambda row: race_codes[row['CAN_RACE']], axis=1)
        df['stratum'] = df.apply(lambda row: "{}-{}-{}".format(race_codes[row['DON_RACE']], race_codes[row['CAN_RACE']], int(row['event'])), axis=1)
    
        df = df[['surv', 'event',
                     'DON_RACE', 'DON_A1', 'DON_A2', 'DON_B1', 'DON_B2', "DON_C1", "DON_C2", "DON_DQ1", "DON_DQ2", 'DON_DR1', 'DON_DR2', 
                     'CAN_RACE', 'REC_A1', 'REC_A2', 'REC_B1', 'REC_B2', "REC_C1", "REC_C2", "REC_DQ1", "REC_DQ2", 'REC_DR1', 'REC_DR2',
                     'DON_RACE_CODE', 'CAN_RACE_CODE', 'stratum',
                     'rec_age_young', 'rec_age_mid', 'rec_age_golden', 'rec_age_senior',
                     'rec_sex', 'rec_race_black', 'rec_race_other', 'rec_dialysis_months',
                     'rec_ins_private', 'rec_ins_none',
                     'don_age_adult', 'don_age_golden', 'don_age_senior', 'don_sex',
                     'don_race_black', 'don_race_other', 'don_type_standard', 'don_type_expanded',
                     'trans_wr_large', 'trans_wr_same',
                     'trans_era_mid', 'trans_era_late', 'REC_COLD_ISCH_TM',
                     'trans_induction_tmg', 'trans_induction_campath', 'trans_induction_IL2', 
                     'trans_calcineurin_cyclo', 'trans_calcineurin_tacro', 'trans_steroids']]
        df.reset_index(inplace=True, drop=True)
        # df.to_csv("data/SRTR_2000-2015_gctc_quant.csv", index=False)
    
        # Low to High imputation

        print("Imputing donors")
        df[DON_HLA] = parallel_imputation(df, 'DON_RACE', DON_HLA)
        print("Imputing recipients")
        df[REC_HLA] = parallel_imputation(df, 'CAN_RACE', REC_HLA)
        
        df.dropna(inplace=True)
    
        # Get eplets and their correlations
    
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            eplets_I = df.apply(get_eplets, axis=1, result_type='expand', loci = loci, eplet_dict = eplet_dict, hla_type = "I").fillna(0)
            eplets_I = eplets_I.reindex(sorted(eplets_I.columns), axis=1)
            print("Type I:", eplets_I.shape)
            
            eplets_II = df.apply(get_eplets, axis=1, result_type='expand', loci = loci, eplet_dict = eplet_dict, hla_type = "II").fillna(0)
            eplets_II = eplets_II.reindex(sorted(eplets_II.columns), axis=1)
            print("Type II:", eplets_II.shape)

            eplet_load = eplets_I.sum(axis=1) + eplets_II.sum(axis=1)
            el_levels = np.quantile(eplet_load, [p/10 for p in range(10)])
            df['eplet_load'] = eplet_load
            df = df.join(pd.get_dummies(np.searchsorted(el_levels, eplet_load), prefix='eplet_load_decile', drop_first=True))
            abv_I = [e for e in abv_list if e in eplets_I.columns]
            abv_II = [e for e in abv_list if e in eplets_II.columns]
            abv_load = eplets_I[abv_I].sum(axis=1) + eplets_II[abv_II].sum(axis=1)
            df['abv_load'] = abv_load
            abv_levels = np.quantile(abv_load, [p/10 for p in range(10)])
            df = df.join(pd.get_dummies(np.searchsorted(abv_levels, abv_load), prefix='abv_load_decile', drop_first=True))
    
        low_freq_eplets_I = eplets_I.columns[eplets_I.var() < VAR_CUTOFF]
        print(low_freq_eplets_I)
        eplets_I.drop(low_freq_eplets_I, axis=1, inplace=True)
    
        low_freq_eplets_II = eplets_II.columns[eplets_II.var() < VAR_CUTOFF]
        print(low_freq_eplets_II)
        eplets_II.drop(low_freq_eplets_II, axis=1, inplace=True)
    
        eplet_dist_I  = 1-np.abs(eplets_I.corr().dropna(how='all', axis=0).dropna(how='all', axis=1))
        eplet_dist_II = 1-np.abs(eplets_II.corr().dropna(how='all', axis=0).dropna(how='all', axis=1))
    
        # Determine WCNA cluster width for condition number roughly 1000
    
        cluster_width_lb = 0
        cluster_width_ub = 0.2
        # This is an arbitrary matrix conditioning target for successful convergence with minimal Ridge penalty
        # Small statistical analyses typically warn against 30 or higher
        # High dimensional machine learning settings warn against 1000 or higher
        cn_target = 1000.
        while cluster_width_lb < cluster_width_ub - 1e-3:
            cluster_width = (cluster_width_ub - cluster_width_lb) / 2 + cluster_width_lb
            print("Testing", cluster_width)
            cn = condition_number(get_profiled(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width))
            print("Condition number:", cn)
            print()
            if cn < cn_target:
                cluster_width_ub = cluster_width
            elif cn > cn_target:
                cluster_width_lb = cluster_width
            else:
                break
        cluster_width_1000 = np.round(cluster_width_ub,3)
        print("Final cluster width:", cluster_width_1000)
    
        # Apply profiles with given cluster width
    
        df = df.drop(DON_HLA + REC_HLA + ['DON_RACE', 'CAN_RACE', 'trans_induction_IL2'], axis=1)\
                         .join(get_profiled(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width_1000)).copy()
    
        for c in df.columns:
            if df[c].dtype == "float64" and len(df[c].unique()) == 2:
                df[c] = df[c].astype("bool")
    
        with open(dataset_name + '_profiles.pkl', 'wb') as fp:
            pickle.dump(get_profiles(eplets_I, eplet_dist_I, eplets_II, eplet_dist_II, cluster_width_1000), fp)
    
        # 80-20 train test split stratified by race and censorship
            
        df_train, df_test, _, _ = train_test_split(df, df['surv'], test_size=0.2, stratify = df['stratum'], random_state=1914)
        df_train.drop(['stratum'], axis=1, inplace=True)
        df_test.drop(['stratum'], axis=1, inplace=True)

        # Fill eplet load 0 deciles with False
        df_train.fillna(False, inplace=True)
        df_test.fillna(False, inplace=True)
    
        df_train.to_parquet("data/" + dataset_name +"_train.parquet")
        df_test.to_parquet("data/" + dataset_name +"_test.parquet")

    return df_train, df_test


def augment_data(df_aug, results, augment_type="marginal", recond=True):
    df_out = df_aug.drop(race_cols, axis=1).copy()
    # Append extra columns for the interactions
    for ir in results:
        if (augment_type == "joint"):
            if ir[-3:] not in ["DON", "CAN"]:
                don_race, can_race = ir.split("_")
                pair_idx = (df_aug['DON_RACE_CODE'] == don_race) & (df_aug['CAN_RACE_CODE'] == can_race)
                pair_append = df_aug.loc[pair_idx, results[ir]['model'].index.drop('likelihood')]
                df_out = df_out.join(pair_append, how='left', rsuffix=("_" + ir))
        else:
            if "_DON" in ir:
                don_idx = df_aug['DON_RACE_CODE'] == ir[0:3]
                don_append = df_aug.loc[don_idx, results[ir]['model'].index.drop('likelihood')]
                df_out = df_out.join(don_append, how='left', rsuffix=("_" + ir))
        
            if "_CAN" in ir:
                can_idx = df_aug['CAN_RACE_CODE'] == ir[0:3]
                can_append = df_aug.loc[can_idx, results[ir]['model'].index.drop('likelihood')]
                df_out = df_out.join(can_append, how='left', rsuffix=("_" + ir))

    # Fill missing values for records missing the interaction
    for col in df_out.columns:
        if df_out[col].isnull().sum() > 0:
            if col[-3:] == "DON" or col[-3:] == "CAN" or col[-3:] in VIABLE_RACE_CODES:
                base_col = col[:-8]
                base_type = df_out[base_col].dtype
                if base_type == "bool":
                    df_out[col] = df_out[col].fillna(False)
                    df_out.dtype = base_type
                elif base_type == "float64":
                    placeholder = df_out[col].mean()
                    df_out[col] = df_out[col].fillna(placeholder)
                    df_out.dtype = base_type
            else:
                print("Base column missing data:", col)

    if recond:
        return(recondition(df_out))
    else:
        return(df_out)



def fit_model(df_model, dataset_name = "gctc", fit_mode = "penalized"):
    marginal_file = 'data/' + dataset_name + '_' + fit_mode + '_marginal_model.pkl'
    joint_file = 'data/' + dataset_name + '_' + fit_mode + '_joint_model.pkl'

    if exists(marginal_file) and exists(joint_file):
        with open(joint_file, 'rb') as fp:
            cph_joint = pickle.load(fp)
        with open(marginal_file, 'rb') as fp:
            cph_marginal = pickle.load(fp)
    else:
    
        model_std = df_model.drop(['surv', 'event']+race_cols, axis=1).std()
    
        # Fit baseline ridge
        file = 'data/' + dataset_name + '_' + fit_mode + '_w.pkl'
        if exists(file):
            with open(file, 'rb') as fp:
                w = pickle.load(fp)
        else:
            cph_w = CoxPHFitter(penalizer=1/df_model.shape[0], l1_ratio=0)
            cph_w.fit(df_model.drop(race_cols, axis=1), duration_col='surv', event_col='event')
    
    
            # Z * Beta = X * param
            # Z = X / X.std
            # Beta = param * X.std
            w = np.array(1/np.abs(cph_w.params_*model_std))
            with open(file, 'wb') as fp:
                pickle.dump(w, fp)
    
        solution_path = [2 ** p for p in range(-19,1)]
        file = 'data/' + dataset_name + '_' + fit_mode + '_best_penalizer.pkl'
        if exists(file):
            with open(file, 'rb') as fp:
                best_penalizer = pickle.load(fp)
        else:
            np.random.seed(62675)
            FOLDS = 10
            fold = pd.Series(np.random.randint(FOLDS, size = df_model.shape[0]), index=df_model.index, name="fold")
            df_model['fold'] = fold
        
            print("Fitting Ridge Baselines")
            df_model_cv = df_model.drop(race_cols, axis=1)
            w_cv = Parallel(n_jobs=10)(delayed(cv_ridge_baseline)(df_model_cv, f) for f in range(FOLDS))
            
            penalty_quality = np.zeros((FOLDS, len(solution_path)))
            for f in range(FOLDS):
                print("Fitting fold", f)
                df_cv_train = df_model[df_model['fold'] != f].drop(['fold']+race_cols, axis=1)
                df_cv_test = df_model[df_model['fold'] == f].drop(['fold']+race_cols, axis=1)
                penalty_quality[f] = np.array(Parallel(n_jobs=10)(delayed(parallel_cox)(df_cv_train, w_cv[f], s, df_cv_test, fit_mode = fit_mode) for s in solution_path))
            df_model.drop(['fold'], axis=1, inplace=True)
            
            best_penalizer = solution_path[np.argmax(penalty_quality.mean(axis=0))]
            with open(file, 'wb') as fp:
                pickle.dump(best_penalizer, fp)
        print("Best penalizer:", best_penalizer, best_penalizer * df_model.shape[0])
    
    
        # Fit with only basic characteristics
        file = 'data/' + dataset_name + '_' + fit_mode + '_cph_base.pkl'
        if exists(file):
            with open(file, 'rb') as fp:
                cph_base = pickle.load(fp)
        else:
            cph_base, _ = parallel_cox(df_model.drop(race_cols, axis=1).iloc[:,0:32], w[0:30], best_penalizer)
            with open(file, 'wb') as fp:
                pickle.dump(cph_base, fp)
    
        # Adaptive Lasso fit without race interactions
        file = 'data/' + dataset_name + '_' + fit_mode + '_cph_best.pkl'
        if exists(file):
            with open(file, 'rb') as fp:
                cph_best = pickle.load(fp)
        else:
            cph_best, _ = parallel_cox(df_model.drop(race_cols, axis=1), w, best_penalizer)
            with open(file, 'wb') as fp:
                pickle.dump(cph_best, fp)
    
        df_model['likelihood'] = cph_best.predict_log_partial_hazard(df_model)
    
        interaction_results = {}
        file = 'data/' + dataset_name + '_' + fit_mode + '_interaction_results.pkl'
        if exists(file):
            with open(file, 'rb') as fp:
                interaction_results = pickle.load(fp)
            
        for r in VIABLE_RACE_CODES:
            print("Fitting Donors:", r)
            if r + "_DON" not in interaction_results:
                df_train_r = df_model[df_model['DON_RACE_CODE'] == r].drop(race_cols, axis=1)
                print(df_train_r.shape)
                interaction_results[r + "_DON"] = fit_interaction(df_train_r)
                with open(file, 'wb') as fp:
                    pickle.dump(interaction_results, fp)
            print("Fitting Recipients:", r)
            if r + "_CAN" not in interaction_results:
                df_train_r = df_model[df_model['CAN_RACE_CODE'] == r].drop(race_cols, axis=1)
                print(df_train_r.shape)
                interaction_results[r + "_CAN"] = fit_interaction(df_train_r)
                with open(file, 'wb') as fp:
                    pickle.dump(interaction_results, fp)
    
        for r in VIABLE_RACE_CODES:
            for rr in VIABLE_RACE_CODES:
                print("Fitting Pairing:", r, rr)
                if r + "_" + rr not in interaction_results:
                    df_train_r = df_model[(df_model['DON_RACE_CODE'] == r) & (df_model['CAN_RACE_CODE'] == rr)].drop(race_cols, axis=1)
                    print(df_train_r.shape)
                    interaction_results[r + "_" + rr] = fit_interaction(df_train_r)
                    with open(file, 'wb') as fp:
                        pickle.dump(interaction_results, fp)
    
        # Likelihood no longer required
        
        df_model.drop(['likelihood'], axis=1, inplace=True)
    
        # Build augmented dataframes that include interactions
    
        df_marginal = augment_data(df_model, interaction_results, augment_type="marginal")
        df_joint    = augment_data(df_model, interaction_results, augment_type="joint")
    
        # Fit the unbiased models
    
        #
        # Marginal
        #
        
        if exists(marginal_file):
            with open(marginal_file, 'rb') as fp:
                cph_marginal = pickle.load(fp)
        else:
            cph_marginal = CoxPHFitter(penalizer=0)
            cph_marginal.fit(df_marginal, duration_col='surv', event_col='event')
            with open(marginal_file, 'wb') as fp:
                pickle.dump(cph_marginal, fp)
    
        #
        # Joint
        #
        
        if exists(joint_file):
            with open(joint_file, 'rb') as fp:
                cph_joint = pickle.load(fp)
        else:
            cph_joint = CoxPHFitter(penalizer=0)
            cph_joint.fit(df_joint, duration_col='surv', event_col='event')
            with open(joint_file, 'wb') as fp:
                pickle.dump(cph_joint, fp)

    return(cph_marginal, cph_joint)

def evaluate_model(df_train_ev, df_test_ev, cph_ev, 
                   dataset_name="gctc", fit_mode="penalized", augment_type="marginal", 
                   random_state = 13053, B = 1000):
    np.random.seed(random_state)
    cph_ev.print_summary(digits=3)

    file = 'data/' + dataset_name + '_' + fit_mode + '_interaction_results.pkl'
    if exists(file):
        with open(file, 'rb') as fp:
            interaction_results = pickle.load(fp)
    else:
        return(False)

    file = 'data/' + dataset_name + '_' + fit_mode + '_' + augment_type + '_bci.pkl'
    if exists(file):
        with open(file, 'rb') as fp:
            bci_train, bci_test = pickle.load(fp)
    else:
        df_train_aug = augment_data(df_train_ev, interaction_results, augment_type, False)
        df_test_aug  = augment_data(df_test_ev[df_train_ev.columns], interaction_results, augment_type, False)
    
        bci_train = Parallel(n_jobs=10)(delayed(bootstrap_c_index)(df_train_aug, cph_ev) for b in range(B))
        bci_test = Parallel(n_jobs=10)(delayed(bootstrap_c_index)(df_test_aug, cph_ev) for b in range(B))
        with open(file, 'wb') as fp:
            pickle.dump((bci_train,bci_test), fp)
        
    print("Training c-index:", np.quantile(bci_train, (0.05, 0.5, 0.95)))
    print("Testing c-index:", np.quantile(bci_test, (0.05, 0.5, 0.95)))
    print("Starting Features:", len(cph_ev.params_))
    print("Remaining Features:",(np.abs(cph_ev.params_) > 1e-8).sum())

    ev_model = cph_ev.params_[(cph_ev.confidence_intervals_['95% lower-bound'] > 0) | (cph_ev.confidence_intervals_['95% upper-bound'] < 0)]
    print("Significant Features:", len(ev_model))
    with pd.option_context('display.max_rows', None,
                           'display.max_columns', None,
                           'display.precision', 3,
                           ):
        print(np.exp(ev_model.sort_index()))
    return(ev_model, cph_ev.params_[cph_ev.params_ > 1e-8])

def print_row(row, sig_only=False):
    print_str = ""
    row_vals = []
    for c in row:
        if isinstance(c, str):
            row_vals.append(c.replace("REC_COLD_ISCH_TM","REC_ISCH")\
                             .replace("_decile", "_dcl")\
                             .replace(" Type", "")\
                             .replace("expanded", "exp")\
                             .replace("_load", "_ld")\
                             .replace("_senior", "_snr")\
                             .replace("_young", "_yng")\
                             .replace("_golden", "_gld")\
                             .replace("_private", "_priv")\
                             .replace("_black", "_afa")\
                             .replace("eplet_", "ep_")\
                             .replace("_DR", "DR")\
                             .replace("_DQ", "DQ")\
                             .replace("calcineurin_tacro", "c.tacro")\
                             .replace("trans_", "tr_")\
                             .replace("_", " "))
            if (any([abv in c for abv in abv_list])): row_vals[-1] = row_vals[-1] + "\\textsuperscript{*}"
        else:
            row_vals.append(str(np.round(c, 3)))
    # if row['p'] < .001:
    #     row_vals.append("***")
    # elif row['p'] < .01:
    #     row_vals.append("**")
    # elif row['p'] < .05:
    #     row_vals.append("*")
    # elif row['p'] < .1:
    #     row_vals.append("+")
    # else:
    #     row_vals.append("")
    if row['p'] < .05 and not sig_only:
        row_vals[0] = '\\textbf{' + row_vals[0] + '}'
    if row_vals[4] == "0.0": row_vals[4] = "<0.001"
    print_str += (' & '.join(row_vals) + ' \\\\')
    return(print_str)

def print_output(dataset_name="gctc", fit_mode="penalized", augment_type="marginal", sig_only=False):
    model_file = 'data/' + dataset_name + '_' + fit_mode + '_' + augment_type + '_model.pkl'
    if exists(model_file):
        with open(model_file, 'rb') as fp:
            cph_print = pickle.load(fp) 
    else:
        return(False)

    df_output = cph_print.summary[['exp(coef) lower 95%', 'exp(coef)', 'exp(coef) upper 95%', 'p']]
    if sig_only: df_output = df_output[df_output['p'] <= 0.05]
    df_output.columns = ['95% CI LB', 'rel. risk', '95% CI UB', 'p']
    df_output = df_output.sort_values('covariate').reset_index()
    # df_output = np.abs(np.log(df_output[df_output['rel. risk']])) > 1e-8
    print_str = ""

    page_rows = 52
    for i in range(int(np.ceil(df_output.shape[0] / page_rows))):
        start = i*page_rows
        end = (i+1)*page_rows + 1
        if i % 2 == 0:
            print_str += '\\pagebreak\n\\begin{center}\n'
            if dataset_name == "gctc":
                print_str += "Primary "
            else:
                print_str += "Sensitivity "
            if augment_type == "marginal":
                print_str += "Marginal Analysis "
            else:
                print_str += "Joint Analysis "
            if i > 0:
                print_str += "cont."
            print_str += " \\\\\n"
        else:
            print_str += '\\begin{center}\n'
        print_str += '\\begin{tabular}{ l r r r r }\n'
        print_str += " & ".join(['feature', 'LB', 'rel. risk', 'UB', 'p']) + " \\\\\n"
        
        print_str += "\n".join(df_output.iloc[start:end].apply(print_row, sig_only = sig_only, axis=1))
    
        print_str += '\\end{tabular}\n\\end{center}\n'
    
    return(print_str)

def output_profiles(dataset="gctc"):
    profiles_file = dataset + "_profiles.pkl"
    if exists(profiles_file):
        with open(profiles_file, 'rb') as fp:
            profiles = pickle.load(fp) 
    else:
        return(False)
    if dataset == "gctc":
        title = "\\textbf{APPENDIX C:} Primary"
    else:
        title = "\\textbf{APPENDIX F:} Sensitivity"
    output = "\\onecolumn\n\
\\begin{center}\n\
" + title + " Weighted Correlation Network Analysis Profiles \\\\\n\
\\vspace{2mm}\
\\begin{tabular}{ l l }"
    for p in profiles[0]:
        output += "\n\
    Profile I " + str(p) + " & " + ", ".join(profiles[0][p]) + "\\\\"
    for p in profiles[1]:
        output += "\n\
    Profile II " + str(p) + " & " + ", ".join(profiles[1][p]) + "\\\\"

    output += "\n\
\\end{tabular}\n\
\\end{center}\n\
\\twocolumn"
    return(output.replace("_","\_"))

def output_table_1(dataset_name="gctc"):
    raw_file = "data/" + dataset_name +"_raw.parquet"
    if exists(raw_file):
        df_raw = pd.read_parquet(raw_file)
    else:
        tx_ki = pd.read_sas("/home/kidneyData/DATA_SP_2020/tx_ki.sas7bdat", encoding='latin1')
        immuno = pd.read_sas("/home/kidneyData/DATA_SP_2020/immuno.sas7bdat", encoding='latin1')
        rec_histo = pd.read_sas("/home/kidneyData/DATA_SP_2020/rec_histo.sas7bdat", encoding='latin1')
        
        stats = {}
        ### Data filters
    
        if dataset_name == "contemporary":
            # From 1/1/2000 to 1/1/2021
            df = tx_ki.loc[(tx_ki['REC_TX_DT'] <= pd.Timestamp(2021,1,1)) &
                       (tx_ki['REC_TX_DT'] >= pd.Timestamp(2000,1,1)),].copy()
        else: # default to gctc
            # From 1/1/2000 to 1/1/2015
            df = tx_ki.loc[(tx_ki['REC_TX_DT'] <= pd.Timestamp(2015,1,1)) &
                       (tx_ki['REC_TX_DT'] >= pd.Timestamp(2000,1,1)),].copy()
            
        print("Initial: ", df.shape[0])
        stats["Initial"] = df.shape[0]
        cur_rows = df.shape[0]
        
        rec_histo_small = rec_histo[['REC_HISTO_TX_ID','DON_CW1','DON_CW2','DON_DQW1','DON_DQW2','REC_CW1','REC_CW2','REC_DQW1','REC_DQW2']]
        df = pd.merge(df, rec_histo_small, how="left", on="REC_HISTO_TX_ID")
        df = df.rename(columns={'DON_CW1': 'DON_C1', 'DON_CW2': 'DON_C2', 'DON_DQW1': 'DON_DQ1', 'DON_DQW2': 'DON_DQ2',
                                'REC_CW1': 'REC_C1', 'REC_CW2': 'REC_C2', 'REC_DQW1': 'REC_DQ1', 'REC_DQW2': 'REC_DQ2'
                               })
        print("With expanded HLA: ", df.shape[0])
        stats["Expanded HLA"] = df.shape[0]
        cur_rows = df.shape[0]
                 
        # Not primary failure
        df = df[df['REC_FAIL_CAUSE_TY'] != 103]
        print("Primary non-function: ", df.shape[0], cur_rows - df.shape[0])
        stats["Non-function"] = df.shape[0]
        cur_rows = df.shape[0]
        
        ### Clean race categories
        
        race_dict = {   8:    "White",
                        16:   "Black or African American",
                        32:   "American Indian or Alaska Native",
                        64:   "Asian",
                        128:  "Native Hawaiian or Other Pacific Islander",
                        256:  "Arab or Middle Eastern",
                        512:  "Indian Sub-continent",
                        2000: "Hispanic/Latino"
        }
    
        def updateRace(x):
            if x in race_dict.values():
                return x
            elif x in race_dict:
                return race_dict[x]
            else:
                return "Generic"
        # df.loc[:, ("DON_RACE")] = df["DON_RACE"].apply(lambda x: updateRace(x))
    
        df.loc[:,('DON_RACE')] = [updateRace(a) for a  in df["DON_RACE"]]
        df.loc[:,('CAN_RACE')] = [updateRace(a) for a  in df["CAN_RACE"]]
        
        don_race_idx = df.columns.get_loc('DON_RACE')
        don_hla_idx  = df.columns.get_loc('DON_A1')
        rec_race_idx = df.columns.get_loc('CAN_RACE')
        rec_hla_idx  = df.columns.get_loc('REC_A1')
        
        # Donor and recipient have some HLA measured default missing to White because imputation isn't better
        df = df[df[DON_HLA].notnull().any(axis=1) & df[REC_HLA].notnull().any(axis=1)]
        print("Valid HLA: ",df.shape[0], cur_rows - df.shape[0])
        stats["Valid HLA"] = df.shape[0]
        cur_rows = df.shape[0]
    
        if dataset_name == "contemporary":
            pass
        else: # default to gctc
            # No PRA allowed
            df = df[df['CAN_LAST_SRTR_PEAK_PRA'].notnull()]
            print("PRA present", df.shape[0], cur_rows - df.shape[0])
            stats["PRA"] = df.shape[0]
            cur_rows = df.shape[0]
            
            # No PRA allowed
            df = df[~(df['CAN_LAST_SRTR_PEAK_PRA'] > 0.0)]
            print("With PRA = 0.0", df.shape[0], cur_rows - df.shape[0])
            stats["PRA > 0"] = df.shape[0]
            cur_rows = df.shape[0]
        
    
        # Survived day of transplant
        df = df[(df['REC_TX_DT'] != df['TFL_GRAFT_DT'])]
        print("Survived day of transplant", df.shape[0], cur_rows - df.shape[0])
        stats["First Day"] = df.shape[0]
        cur_rows = df.shape[0]
    
        # Remove duplicates
        df.drop_duplicates(inplace=True)
        print("Deduplicated: ", df.shape[0], cur_rows - df.shape[0])
        stats["Deduplicated"] = df.shape[0]
        cur_rows = df.shape[0]
        
        # with open('data/' + dataset_name + '_data_filters.pkl', 'wb') as fp:
        #     pickle.dump(stats, fp)
    
        # Time on dialysis (Highly suspect. Use with caution.)
        df['rec_dialysis_months'] = (df['REC_TX_DT'] - df['REC_DIAL_DT'])/np.timedelta64(1, 'W')/4 # Cannot measure ambiguous times like months
        
        ### Survival time and event determination
        
        # The event is return to dialysis or retransplantation (many alive with failed graft but not in this list)
        surv_cols = ['TFL_LAFUDATE', 'TFL_GRAFT_DT', 'REC_FAIL_DT', 'PERS_RELIST', 'PERS_RETX', 'REC_RESUM_MAINT_DIAL_DT']
        df['surv'] = (pd.to_datetime(df[surv_cols].min(axis=1)) - pd.to_datetime(df['REC_TX_DT'])) / np.timedelta64(1, 'D')
        df['event'] = (df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") | (df['TFL_GRAFT_DT'].notnull())
        print("Pure DCGF:", ((df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R")).sum())
        print("Total Events:", ((df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") | (df['TFL_GRAFT_DT'].notnull())).sum())
        # df['event'] = (df['REC_RESUM_MAINT_DIAL'] == "Y") | (df['TFL_LASTATUS'] == "R") # DCGF
        # print("Mysterious graft failures:", (df['TFL_GRAFT_DT'].notnull() & ~df['TFL_DEATH_DT'].notnull() & ~df['event']).sum())
    
        df = df[['surv', 'event',
                 'DON_RACE', 'DON_A1', 'DON_A2', 'DON_B1', 'DON_B2', "DON_C1", "DON_C2", "DON_DQ1", "DON_DQ2", 'DON_DR1', 'DON_DR2', 
                 'CAN_RACE', 'REC_A1', 'REC_A2', 'REC_B1', 'REC_B2', "REC_C1", "REC_C2", "REC_DQ1", "REC_DQ2", 'REC_DR1', 'REC_DR2',
                 'REC_AGE_AT_TX', 'CAN_GENDER', 'REC_TX_DT', 'REC_DIAL_DT',
                 'REC_PRIMARY_PAY', 'DON_AGE', 'DON_GENDER', 'DON_TY', 'DON_EXPAND_DON_KI',
                 'DON_WGT_KG', 'CAN_WGT_KG', 'REC_COLD_ISCH_TM', 'rec_dialysis_months', 'TRR_ID']]
        df = df.loc[:,~df.columns.duplicated()].copy()
    
        # imputable = ['DON_RACE', 'CAN_RACE', 'REC_AGE_AT_TX', 'CAN_GENDER', 'REC_PRIMARY_PAY', 'REC_AGE_AT_TX', 'DON_GENDER', 
        #              'DON_TY', 'DON_EXPAND_DON_KI', 'DON_WGT_KG', 'CAN_WGT_KG', 'REC_COLD_ISCH_TM', 'rec_dialysis_months']
    
        # print(df[imputable].isnull().sum())
        # df.reset_index(inplace=True, drop=True)
    
        # # Formerly the break between unquantized and quantized datasets
    
        # # Identify categorical columns and make them numeric for the imputation
        # is_object = df[imputable].dtypes == "object"
        # need_dummies = is_object[is_object].index
        # df_dummies = pd.get_dummies(df[need_dummies], drop_first = True)*1
        # impute_raw = [c for c in imputable if c not in need_dummies]
        # df_impute = df[impute_raw].join(df_dummies)
    
        # # Multiple imputation via connected equations
        # mice = IterativeImputer(estimator=LinearRegression(), imputation_order="roman")
        # np_imputed = mice.fit_transform(df_impute)
        # df_imputed = pd.DataFrame(np_imputed, columns = df_impute.columns)
        # df_imputed = df_imputed.loc[:,~df_imputed.columns.duplicated()].copy()
    
        # # Nothing categorical needed to be imputed
        # for c in impute_raw:
        #     df[c] = df_imputed[c]
    
        df['REC_TX_DT'] = pd.to_datetime(df['REC_TX_DT'])
        
        ### Map recipient characteristics
        # Ages:
        df['rec_age_young']  = df['REC_AGE_AT_TX'] <= 14
        df['rec_age_mid']    = (df['REC_AGE_AT_TX'] > 14) & (df['REC_AGE_AT_TX'] <= 24)
        # rec_age_adult  = (df['REC_AGE_AT_TX'] > 24) & (df['REC_AGE_AT_TX'] <= 44) REF
        df['rec_age_golden'] = (df['REC_AGE_AT_TX'] > 44) & (df['REC_AGE_AT_TX'] <= 64)
        df['rec_age_senior'] =  df['REC_AGE_AT_TX'] > 64
    
        # Sex
        df['rec_sex'] = df['CAN_GENDER'] == 'F'
    
        # Race
        #rec_race_White = df['CAN_RACE'] == "White" REF
        df['rec_race_black'] = df['CAN_RACE'] == "Black or African American"
        df['rec_race_other'] = ~(df['CAN_RACE'] == "Black or African American") & ~(df['CAN_RACE'] == "White")
    
        # Insurance type
        df['rec_ins_private'] = df['REC_PRIMARY_PAY'] == 1 
        #rec_ins_public = df['REC_PRIMARY_PAY'].isin([2,3,4,5,6,7,12,13,14]) REF
        df['rec_ins_none'] = df['REC_PRIMARY_PAY'].isin([8,9,10,11,15])
    
    
        ### Map donor characteristics
    
        # Ages:
        # don_age_young  = df['DON_AGE_AT_TX'] <= 35 REF
        df['don_age_adult']  = (df['DON_AGE'] > 35) & (df['DON_AGE'] <= 45)
        df['don_age_golden'] = (df['DON_AGE'] > 45) & (df['DON_AGE'] <= 55)
        df['don_age_senior'] =  df['DON_AGE'] > 55
    
        # Sex
        df['don_sex'] = df['DON_GENDER'] == 'F'
    
        # Race
        #don_race_White = df['DON_RACE'] == "White" REF
        df['don_race_black'] = df['DON_RACE'] == "Black or African American"
        df['don_race_other'] = ~(df['DON_RACE'] == "Black or African American") & ~(df['DON_RACE'] == "White")
    
        # Donor type
        #don_type_living = df['DON_TY'] == "L" REF
        df['don_type_standard'] = (df['DON_TY'] == "C") & (df['DON_EXPAND_DON_KI'] == 0.0)
        df['don_type_expanded'] = (df['DON_TY'] == "C") & (df['DON_EXPAND_DON_KI'] == 1.0)
    
        ### Map Transplant characteristics 
        # Donor recipient weight ratio
        wr = df['DON_WGT_KG'].astype(float) / df['CAN_WGT_KG'].astype(float)
        # df['trans_wr_small'] = wr < .9
        df['trans_wr_large'] = wr > 1
        df['trans_wr_same'] = (wr >= .9) & (wr <= 1)
    
        # Transplant era
        # df['trans_era_early'] = df['REC_TX_DT'] < pd.Timestamp(2005,1,1) REF
        df['trans_era_mid']   = (df['REC_TX_DT'] >= pd.Timestamp(2005,1,1)) & (df['REC_TX_DT'] < pd.Timestamp(2010,1,1))
        df['trans_era_late']  = df['REC_TX_DT'] >= pd.Timestamp(2010,1,1)
    
        # Cold ischemic time
        # REC_COLD_ISCH_TM
    
        # Map drug types
        thymoglobulin = immuno[immuno['REC_DRUG_CD'] == 41][["TRR_ID"]]
        thymoglobulin["trans_induction_tmg"] = 1
    
        campath = immuno[immuno['REC_DRUG_CD'] == 50][["TRR_ID"]]
        campath["trans_induction_campath"] = 1
    
        IL2 = immuno[immuno['REC_DRUG_CD'].isin([20, 27])][["TRR_ID"]]
        IL2["trans_induction_IL2"] = 1
    
        cyclosporine = immuno[immuno['REC_DRUG_CD'].isin([-2, 46, 48])][["TRR_ID"]]
        cyclosporine["trans_calcineurin_cyclo"] = 1
    
        tacrolimus = immuno[immuno['REC_DRUG_CD'].isin([5, 53, 54, 59])][["TRR_ID"]]
        tacrolimus["trans_calcineurin_tacro"] = 1
    
        steroids = immuno[immuno['REC_DRUG_CD'] == 49][["TRR_ID"]]
        steroids["trans_steroids"] = 1
    
        df = pd.merge(df, thymoglobulin, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, campath, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, IL2, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, cyclosporine, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, tacrolimus, how='left', on='TRR_ID').drop_duplicates()
        df = pd.merge(df, steroids, how='left', on='TRR_ID').drop_duplicates()
        df['trans_induction_tmg'].fillna(0, inplace=True)
        df['trans_induction_campath'].fillna(0, inplace=True)
        df['trans_induction_IL2'].fillna(0, inplace=True)
        df['trans_calcineurin_cyclo'].fillna(0, inplace=True)
        df['trans_calcineurin_tacro'].fillna(0, inplace=True)
        df['trans_steroids'].fillna(0, inplace=True)
    
        df['DON_RACE_CODE'] = df.apply(lambda row: race_codes[row['DON_RACE']], axis=1)
        df['CAN_RACE_CODE'] = df.apply(lambda row: race_codes[row['CAN_RACE']], axis=1)
        df['stratum'] = df.apply(lambda row: "{}-{}-{}".format(race_codes[row['DON_RACE']], race_codes[row['CAN_RACE']], int(row['event'])), axis=1)
    
        df = df[['surv', 'event',
                     'DON_RACE', 'DON_A1', 'DON_A2', 'DON_B1', 'DON_B2', 'DON_C1', 'DON_C2', 'DON_DQ1', 'DON_DQ2', 'DON_DR1', 'DON_DR2', 
                     'CAN_RACE', 'REC_A1', 'REC_A2', 'REC_B1', 'REC_B2', 'REC_C1', 'REC_C2', 'REC_DQ1', 'REC_DQ2', 'REC_DR1', 'REC_DR2',
                     'DON_RACE_CODE', 'CAN_RACE_CODE', 'stratum',
                     'rec_age_young', 'rec_age_mid', 'rec_age_golden', 'rec_age_senior',
                     'rec_sex', 'rec_race_black', 'rec_race_other', 'rec_dialysis_months',
                     'rec_ins_private', 'rec_ins_none', 'REC_PRIMARY_PAY',
                     'don_age_adult', 'don_age_golden', 'don_age_senior', 'don_sex',
                     'don_race_black', 'don_race_other', 'don_type_standard', 'don_type_expanded',
                     'trans_wr_large', 'trans_wr_same', 'DON_WGT_KG', 'CAN_WGT_KG',
                     'trans_era_mid', 'trans_era_late', 'REC_COLD_ISCH_TM',
                     'trans_induction_tmg', 'trans_induction_campath', 'trans_induction_IL2', 
                     'trans_calcineurin_cyclo', 'trans_calcineurin_tacro', 'trans_steroids']]
        df.reset_index(inplace=True, drop=True)

        has_HLA = ((df['DON_A1'].notnull()  | df['DON_A2'].notnull() | df['DON_B1'].notnull()  | df['DON_B2'].notnull()  | 
                    df['DON_C1'].notnull()  | df['DON_C2'].notnull() | df['DON_DQ1'].notnull() | df['DON_DQ2'].notnull() | 
                    df['DON_DR1'].notnull() | df['DON_DR2'].notnull()) & 
                   (df['REC_A1'].notnull()  | df['REC_A2'].notnull() | df['REC_B1'].notnull()  | df['REC_B2'].notnull()  | 
                    df['REC_C1'].notnull()  | df['REC_C2'].notnull() | df['REC_DQ1'].notnull() | df['REC_DQ2'].notnull() | 
                    df['REC_DR1'].notnull() | df['REC_DR2'].notnull()))
        print(df.shape)
        df_raw = df[has_HLA].copy()
        print(df_raw.shape)
        df_raw.to_parquet("data/" + dataset_name +"_raw.parquet")
    output = ""

    N = df_raw.shape[0]

    output += "\n\
\\begin{table}\n\
\\footnotesize\n\
\\begin{center}\n\
\\begin{tabular}{ l c c c }\n\
Variable &                           & $n$ & \% \\\\\n\
         & Recipient Characteristics &     &    \\\\\n\
    "
    rec_age_young = df_raw['rec_age_young'].sum()
    rec_age_mid = df_raw['rec_age_mid'].sum()
    rec_age_ref = (~(df_raw['rec_age_young'] | df_raw['rec_age_mid'] | df_raw['rec_age_golden'] | df_raw['rec_age_senior'])).sum()
    rec_age_golden = df_raw['rec_age_golden'].sum()
    rec_age_senior = df_raw['rec_age_senior'].sum()
    output += "\n\
Age at transplantation (y) & 0-14        & " + f'{rec_age_young:,}' +  " & " + str(np.round(rec_age_young/N*100, 1)) + " \\\\\n\
                           & 15-24       & " + f'{rec_age_mid:,}' +    " & " + str(np.round(rec_age_mid/N*100, 1)) + " \\\\\n\
                           & 25-44 (Ref) & " + f'{rec_age_ref:,}' +    " & " + str(np.round(rec_age_ref/N*100, 1)) + " \\\\\n\
                           & 45-64       & " + f'{rec_age_golden:,}' + " & " + str(np.round(rec_age_golden/N*100, 1)) + " \\\\\n\
                           & $\geq$65    & " + f'{rec_age_senior:,}' + " & " + str(np.round(rec_age_senior/N*100, 1)) + " \\\\\n"

    rec_sex_male   = (df_raw['rec_sex']==0).sum()
    rec_sex_female = (df_raw['rec_sex']==1).sum()
    output += "\n\
Sex & Male (Ref) & " + f'{rec_sex_male:,}' + " & " + str(np.round(rec_sex_male/N*100,1)) + " \\\\\n\
    & Female     & " + f'{rec_sex_female:,}' + " & " + str(np.round(rec_sex_female/N*100,1)) + " \\\\\n"

    rec_race_CAU = (df_raw['CAN_RACE_CODE'] == "CAU").sum()
    rec_race_AFA = (df_raw['CAN_RACE_CODE'] == "AFA").sum()
    rec_race_other = (~((df_raw['CAN_RACE_CODE'] == "CAU") | (df_raw['CAN_RACE_CODE'] == "AFA"))).sum()
    output += "\n\
Self-reported race & Caucasian (Ref)  & " + f'{rec_race_CAU:,}' + " & " + str(np.round(rec_race_CAU/N*100,1)) + " \\\\\n\
                   & African American & " + f'{rec_race_AFA:,}' + " & " + str(np.round(rec_race_AFA/N*100,1)) + " \\\\\n\
                   & Other            & " + f'{rec_race_other:,}' + " & " + str(np.round(rec_race_other/N*100,1)) + " \\\\\n"
    
    dialysis_missing = df_raw['rec_dialysis_months'].isnull().sum()
    output += "\n\
Time on dialysis & \\textit{Mean, SD} & " + str(np.round(df_raw['rec_dialysis_months'].mean(), 1)) + " & " + str(np.round(df_raw['rec_dialysis_months'].std(), 1)) + " \\\\\n\
                 & Missing    & " + f'{dialysis_missing:,}' + " & " + str(np.round(dialysis_missing/N*100, 1)) + " \\\\\n"

    rec_ins_private = df_raw['rec_ins_private'].sum()
    rec_ins_public = df_raw['REC_PRIMARY_PAY'].isin([2,3,4,5,6,7,12,13,14]).sum()
    rec_ins_none = df_raw['rec_ins_none'].sum()
    rec_ins_missing = N - rec_ins_private - rec_ins_public - rec_ins_none
    output += "\n\
Insurance type & None         & " + f'{rec_ins_none:,}' + " & " + str(np.round(rec_ins_none/N*100,1)) + " \\\\\n\
               & Private      & " + f'{rec_ins_private:,}' + " & " + str(np.round(rec_ins_private/N*100,1)) + " \\\\\n\
               & Public (Ref) & " + f'{rec_ins_public:,}' + " & " + str(np.round(rec_ins_public/N*100,1)) + " \\\\\n\
               & Missing      & " + f'{rec_ins_missing:,}' + " & " + str(np.round(rec_ins_missing/N*100,1)) + " \\\\\n"

    
    
    output += "\n\
         & Donor Characteristics &     &    \\\\\n"

    don_age_adult  = df_raw['don_age_adult'].sum()
    don_age_golden = df_raw['don_age_golden'].sum()
    don_age_senior = df_raw['don_age_senior'].sum()
    don_age_ref = (~(df_raw['don_age_adult'] | df_raw['don_age_golden'] | df_raw['don_age_senior'])).sum()
    output += "\n\
Age (y) & $\leq$35 (Ref) & " + f'{don_age_ref:,}' +  " & " + str(np.round(don_age_ref/N*100, 1)) + " \\\\\n\
        & 36-45          & " + f'{don_age_adult:,}' +    " & " + str(np.round(don_age_adult/N*100, 1)) + " \\\\\n\
        & 46-55          & " + f'{don_age_golden:,}' + " & " + str(np.round(don_age_golden/N*100, 1)) + " \\\\\n\
        & $>$55        & " + f'{don_age_senior:,}' + " & " + str(np.round(don_age_senior/N*100, 1)) + " \\\\\n"

    don_sex_male   = (df_raw['don_sex']==0).sum()
    don_sex_female = (df_raw['don_sex']==1).sum()
    output += "\n\
Sex & Male (Ref) & " + f'{don_sex_male:,}' + " & " + str(np.round(don_sex_male/N*100,1)) + " \\\\\n\
    & Female     & " + f'{don_sex_female:,}' + " & " + str(np.round(don_sex_female/N*100,1)) + " \\\\\n"

    don_race_CAU = (df_raw['DON_RACE_CODE'] == "CAU").sum()
    don_race_AFA = (df_raw['DON_RACE_CODE'] == "AFA").sum()
    don_race_other = (~((df_raw['DON_RACE_CODE'] == "CAU") | (df_raw['DON_RACE_CODE'] == "AFA"))).sum()
    output += "\n\
Self-reported race & Caucasian (Ref)  & " + f'{don_race_CAU:,}' + " & " + str(np.round(don_race_CAU/N*100,1)) + " \\\\\n\
                   & African American & " + f'{don_race_AFA:,}' + " & " + str(np.round(don_race_AFA/N*100,1)) + " \\\\\n\
                   & Other            & " + f'{don_race_other:,}' + " & " + str(np.round(don_race_other/N*100,1)) + " \\\\\n"

    don_type_standard = df_raw['don_type_standard'].sum()
    don_type_expanded = df_raw['don_type_expanded'].sum()
    don_type_living   = N - don_type_standard - don_type_expanded
    output += "\n\
Donor type & Deceased (SCD) & " + f'{don_type_standard:,}' + " & " + str(np.round(don_type_standard/N*100,1)) + " \\\\\n\
           & Deceased (ECD) & " + f'{don_type_expanded:,}' + " & " + str(np.round(don_type_expanded/N*100,1)) + " \\\\\n\
           & Living (Ref)   & " + f'{don_type_living:,}' + " & " + str(np.round(don_type_living/N*100,1)) + " \\\\\n"


    output += "\n\
         & Transplant Characteristics &     &    \\\\\n"

    wr = df_raw['DON_WGT_KG'].astype(float) / df_raw['CAN_WGT_KG'].astype(float)
    trans_wr_small = (wr < .9).sum()
    trans_wr_large = df_raw['trans_wr_large'].sum()
    trans_wr_same  = df_raw['trans_wr_same'].sum()
    trans_wr_missing = N - trans_wr_small - trans_wr_large - trans_wr_same

    output += "\n\
Donor-recipient weight ratio & $< 0.9$ (Ref) & " + f'{trans_wr_small:,}'   + " & " + str(np.round(trans_wr_small/N*100,1)) + " \\\\\n\
                             & $> 1.0$       & " + f'{trans_wr_large:,}'   + " & " + str(np.round(trans_wr_large/N*100,1)) + " \\\\\n\
                             & 0.9-1.0         & " + f'{trans_wr_same:,}'    + " & " + str(np.round(trans_wr_same/N*100,1)) + " \\\\\n\
                             & Missing         & " + f'{trans_wr_missing:,}' + " & " + str(np.round(trans_wr_missing/N*100,1)) + " \\\\\n"

    trans_induction_tmg     = df_raw['trans_induction_tmg'].sum()
    trans_induction_campath = df_raw['trans_induction_campath'].sum()
    trans_induction_IL2     = df_raw['trans_induction_IL2'].sum()
    trans_induction_other = N - trans_induction_tmg - trans_induction_campath - trans_induction_IL2

    output += "\n\
Induction agent & Campath              & " + f'{int(trans_induction_campath):,}'  + " & " + str(np.round(trans_induction_campath/N*100,1)) + " \\\\\n\
                & IL2 Receptor Blocker & " + f'{int(trans_induction_IL2):,}'   + " & " + str(np.round(trans_induction_IL2/N*100,1)) + " \\\\\n\
                & Thymoglobulin        & " + f'{int(trans_induction_tmg):,}'    + " & " + str(np.round(trans_induction_tmg/N*100,1)) + " \\\\\n\
                & Other                & " + f'{int(trans_induction_other):,}' + " & " + str(np.round(trans_induction_other/N*100,1)) + " \\\\\n"

    trans_calcineurin_cyclo = df_raw['trans_calcineurin_cyclo'].sum()
    trans_calcineurin_tacro = df_raw['trans_calcineurin_tacro'].sum()
    trans_calcineurin_none  = N - trans_calcineurin_cyclo - trans_calcineurin_tacro

    output += "\n\
Calcineurin inhibitor & Cyclosporine & " + f'{int(trans_calcineurin_cyclo):,}'   + " & " + str(np.round(trans_calcineurin_cyclo/N*100,1)) + " \\\\\n\
                      & Tacrolimus   & " + f'{int(trans_calcineurin_tacro):,}'   + " & " + str(np.round(trans_calcineurin_tacro/N*100,1)) + " \\\\\n\
                      & None         & " + f'{int(trans_calcineurin_none):,}'    + " & " + str(np.round(trans_calcineurin_none/N*100,1)) + " \\\\\n"

    trans_steroids = df_raw['trans_steroids'].sum()
    output += "\n\
Steroids & Yes & " + f'{int(trans_steroids):,}'   + " & " + str(np.round(trans_steroids/N*100,1)) + " \\\\\n"

    trans_era_mid   = df_raw['trans_era_mid'].sum()
    trans_era_late  = df_raw['trans_era_late'].sum()
    trans_era_ref = N - trans_era_mid - trans_era_late
    
    output += "\n\
Transplant era & 2000-2004 (Ref) & " + f'{trans_era_ref:,}'   + " & " + str(np.round(trans_era_ref/N*100,1)) + " \\\\\n\
               & 2005-2009       & " + f'{trans_era_mid:,}'   + " & " + str(np.round(trans_era_mid/N*100,1)) + " \\\\\n\
               & 2010-2014       & " + f'{trans_era_late:,}'    + " & " + str(np.round(trans_era_late/N*100,1)) + " \\\\\n"    

    cold_isch_missing = df_raw['REC_COLD_ISCH_TM'].isnull().sum()
    output += "\n\
Cold ischemia time (h) & \\textit{Mean, SD} & " + str(np.round(df_raw['REC_COLD_ISCH_TM'].mean(), 1)) + " & " + str(np.round(df_raw['REC_COLD_ISCH_TM'].std(), 1)) + " \\\\\n\
                       & Missing    & " + f'{cold_isch_missing:,}' + " & " + str(np.round(cold_isch_missing/N*100, 1)) + " \\\\\n"
    
    output += "\n\
\\end{tabular}\n\
\\end{center}\n"
    
    if dataset_name == "gctc":
        output += "\n\
\caption{\label{baseline}Baseline characteristics of retrospective cohort members from U.S. Scientific Registry of Transplant Recipients}"
    else:
        output += "\n\
\caption{\label{baseline}Baseline characteristics of sensitivity analysis members from U.S. Scientific Registry of Transplant Recipients}"
    output += "\n\
\\end{table}"
    return(output)
    
    
    
    